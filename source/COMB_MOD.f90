MODULE COMB_MOD
    
implicit none
private


TYPE, PUBLIC :: comb
    
    INTEGER(8) :: zs(1:10)
    INTEGER(1) :: N
    
   contains
     procedure :: output => work
   
END TYPE comb
  
  
  CONTAINS
  
FUNCTION WORK (this,input) RESULT(output)
      
    class(comb), intent(inout) :: this
    INTEGER(8),INTENT(IN) :: input
    
    INTEGER(8) :: output
    
    
    INTEGER(1) :: k
    
    
    output=input-this%zs(this%N)
    
    DO k=this%N,2,-1
             this%zs(k)=this%zs(k-1)             
    END DO
    
    this%zs(1)=input
             
END FUNCTION work

    
    

END MODULE
