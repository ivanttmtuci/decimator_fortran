MODULE INTEGRATOR
    
    implicit none
private


TYPE, PUBLIC :: integr
    
    INTEGER(8) :: z
   
    
   contains
     procedure :: output => work
   
END TYPE integr
  
  
  CONTAINS
  
FUNCTION WORK  (this,input) RESULT(output)
      
    class(integr), intent(inout) :: this
    INTEGER(8),INTENT(IN) :: input
    
    INTEGER(8) :: output   
    
     
    
    output=input+this%z  
    
    this%z=output
    
             
END FUNCTION work
    
    
END MODULE INTEGRATOR