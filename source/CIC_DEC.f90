MODULE CIC_DEC

  USE COMB_MOD
  USE INTEGRATOR
  USE CIC_OUTPUTDATAMOD

implicit none
private


TYPE, PUBLIC ::CIC1_DEC_4TH_ORDER
    
    type(comb) :: comb1(1:5)
    type(integr) ::integr1(1:5)
    type(cic_analysys_out) :: anlys_out
    integer(8) :: stage_out(1:10)=0
    
    integer(2) :: R
    integer(1) :: N   
    INTEGER(2) :: zero_cnt =0
    
        
   contains
  
    procedure :: output_sample => work
    procedure :: output_whole => work_whole
   
    procedure :: state => init
   
END TYPE CIC1_DEC_4TH_ORDER



CONTAINS

!************************************************************
!**********************FUNCTIONS**********************

FUNCTION WORK (this,input)  RESULT(output)
      
    class(CIC1_DEC_4TH_ORDER), intent(inout) :: this    
    INTEGER(8),INTENT(IN) :: input        
    INTEGER(8) :: output  
    INTEGER(1) :: i 
    
  
    INTEGER(8) :: xx
         ! ***********GO HARD******************
     
     IF ( this%zero_cnt==this%R) THEN 
     
                this%zero_cnt=0             !  ����� ����, ��� ����� ����� ��������� R-1 ���� �� ������ ����� 1
                
                this%stage_out(1)= this%integr1(1)%output(input)
             
                DO I=2,5
                   this%stage_out(i)= this%integr1(i)%output(this%stage_out(i-1))  
                END DO   
               
                 
                DO I=6,10
                   this%stage_out(i)= this%comb1(i-5)%output(this%stage_out(i-1))
                END DO    
                                              
                
     ELSE    
              
                                                    
               this%stage_out(1)= this%integr1(1)%output(input)    
                      
                DO I=2,5
                   this%stage_out(i)= this%integr1(i)%output(this%stage_out(i-1))  
                END DO    
                
                
                this%stage_out(10)=0
                
                
               
                                    
     END IF
                 output=this%stage_out(10)
        
      this%zero_cnt= this%zero_cnt+1
  
                ! ����������� ������������� ��������
                DO i=1,10
                        this%anlys_out%stage_value_max(i)=MAX(abs(this%stage_out(i)),abs(this%anlys_out%stage_value_max(i)))                
                END DO
                
                
  
   
    
    !**************STOP HARD************         
                
END FUNCTION WORK



FUNCTION WORK_WHOLE (this,in_arr,out_arr,capacity,theretical_max_cap) RESULT(output)

    class(CIC1_DEC_4TH_ORDER), intent(inout) :: this

    INTEGER(2), INTENT (IN) ::  in_arr(:)

    INTEGER(1), INTENT (IN) ::  capacity ! expected final stage bit capacity (sign included)
    INTEGER(1), INTENT (IN) ::  theretical_max_cap ! estimated msximim final stage bit capacity (sign included)

    INTEGER(2), INTENT (OUT), ALLOCATABLE ::  out_arr(:)

    integer(8) :: length


    INTEGER(8) :: i,j,k

    integer(8) :: input_sample_8
    integer(8) :: output_sample_8
    integer(1) :: shift_need

    INTEGER(8) :: output


    length=size(in_arr)

    shift_need=theretical_max_cap-capacity
    write(*,*) 'was shifted ', shift_need

    k=1
    j=0


    ALLOCATE(out_arr(1:length/this%R) )

    ! ***********GO HARD******************

    DO I = 1, length

        IF (j==this%R) THEN

            input_sample_8=int(in_arr(i),8)

            output_sample_8=this%output_sample(input_sample_8)

            out_arr(k)= int( SHIFTA( output_sample_8,shift_need),2)

            !write(*,*) input_sample_8,output_sample_8

            j=0
            k=k+1

        ELSE

            input_sample_8=int(in_arr(i),8)

            output_sample_8=this%output_sample(input_sample_8)



        END IF

        j=j+1


    END DO


    !**************STOP HARD************

    output=1

END FUNCTION WORK_WHOLE



FUNCTION INIT (this,inp_N,inp_R)  RESULT(state)  ! initialize of COMBS  and INTEGRS
      
    CLASS(CIC1_DEC_4TH_ORDER), INTENT(inout) :: this
    
    INTEGER(2), INTENT(IN) :: inp_N,inp_R
    INTEGER(1) :: state
     
     
      
       
       this%N=inp_N
       this%R=inp_R
       
       this%comb1(1)=comb(0,this%N)
       this%integr1(1)=integr(0)    
       
       this%comb1(2)=comb(0,this%N)
       this%integr1(2)=integr(0)    
              
       this%comb1(3)=comb(0,this%N)
       this%integr1(3)=integr(0)    
       
       this%comb1(4)=comb(0,this%N)
       this%integr1(4)=integr(0)    
       
       this%comb1(5)=comb(0,this%N)
       this%integr1(5)=integr(0)

       state=1   
             
END FUNCTION  INIT



END MODULE
