program main

    USE  CIC_DEC

    implicit none

    character(50)    input_filename_i,input_filename_q
    integer(8) file_len

    integer(2), allocatable :: input_i(:)
    integer(2), allocatable :: input_q(:)

    type(CIC1_DEC_4TH_ORDER) ::CIC4_DEC_I,CIC4_DEC_Q

    integer(2), allocatable :: CIC_DEC_OUT_I(:)
    integer(2), allocatable :: CIC_DEC_OUT_Q(:)

    integer(2) R ! interpolaton coeff
    integer(1) capacity ! expected final stage bit capacity (sign included)
    integer(1) theoretical_max_cap  ! estimated msximim final stage bit capacity (sign included)


    integer(8) status




    !===========MAIN PROGRAM============================

    open(10, file = 'input\spis.txt')
                rewind 10

                read(10,*)   R
                read(10,*)   theoretical_max_cap
                read(10,*)   capacity
                read(10,*)   input_filename_i
                read(10,*)   input_filename_q
                read(10,*)   file_len


   close(10)


   ALLOCATE(input_i(1:file_len) )
   ALLOCATE(input_q(1:file_len) )

   open(10,file=input_filename_i,ACCESS="STREAM",FORM="UNFORMATTED")
   open(11,file=input_filename_q,ACCESS="STREAM",FORM="UNFORMATTED")

   read(10) input_i
   read(11) input_q

   close(10)
   close(11)


   input_i=SHIFTA(input_i,8)/2 +32
   !input_i=64

   status=CIC4_DEC_I%state(int(1,2),int(R,2))
   status=CIC4_DEC_Q%state(int(1,2),int(R,2))

   status=CIC4_DEC_I%output_whole(input_i,CIC_DEC_OUT_I,capacity,theoretical_max_cap)

   open(10,file='output\out.pcm',ACCESS="STREAM",FORM="UNFORMATTED")

   write(10) CIC_DEC_OUT_I

   write(*,*) 'DONE!'


end program main
